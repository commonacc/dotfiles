# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
        source "$HOME/.bashrc"

        "^[[2~": quoted-insert
        "^[[3~": delete-char
        "^[[H": beginning-of-line
        "^[[F": end-of-line
        "^[[5~": beginning-of-history
        "^[[6~": end-of-history
        "^[[2~": quoted-insert
        "^[[3~": delete-char
        "^[[1~": beginning-of-line
        "^[[4~": end-of-line
    fi
fi

#setxkbmap -option caps:super,compose:menu

## execute if you login from tty1
#if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
#    XDG_SESSION_TYPE=x11 startx
#fi

# execute if you login from tty1
if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
    export DESKTOP_SESSION=plasma
    XDG_SESSION_TYPE=wayland startplasma-wayland
fi

. "$HOME/.cargo/env"
