# vim: syntax=config

log-file=~~/log                         #Sets a location for writing a log file, ~~/ translates to ~/.config/mpv

###########
# General #
###########

geometry=50%:50%
no-border                               # no window title bar
msg-module                              # prepend module name to log messages
msg-color                               # color log messages on terminal
term-osd-bar                            # display a progress bar on the terminal
use-filedir-conf                        # look for additional config files in the directory of the opened file
#pause                                   # no autoplay
#keep-open                               # keep the player open when a file's end is reached
autofit-larger=100%x95%                 # resize window in case it's larger than W%xH% of the screen
cursor-autohide-fs-only                 # don't autohide the cursor in window mode, only fullscreen
input-media-keys=no                     # enable/disable OSX media keys
cursor-autohide=1000                    # autohide the curser after 1s
prefetch-playlist=yes
force-seekable=yes

screenshot-format=png
screenshot-png-compression=8
screenshot-template='~/Downloads/%F_%P_%n'

hls-bitrate=max                         # use max quality for HLS streams

#ytdl-format=bestvideo[ext=mp4][height<=?1080]+bestaudio[ext=m4a]/best
ytdl-format="bestvideo+bestaudio/best"
ytdl="yes"
#ytdl_hook-try_ytdl_first=yes
script-opts-append="ytdl_hook-try_ytdl_first=yes"
#script-opts-append="ytdl_hook-exclude='%.webm$|%.ts$|%.mp3$|%.m3u8$|%.m3u$|%.mkv$|%.mp4$|%.VOB$'"
#########
# Cache #
#########

cache=yes
#cache-default=4000000
#cache-backbuffer=2000000
demuxer-max-bytes=547483647

#############
# OSD / OSC #
#############

osd-bar-h=2                             # height of osd bar as a fractional percentage of your screen height
osd-bar-w=60                            # width of " " "
osd-border-color="#FF262626"
osd-border-size="1.5"
osd-color="#FFFFFFFF"
osd-duration="1500"
osd-font-size="21"
osd-font="Liberation Sans:style=Regular"
osd-level=1                             # enable osd and display --osd-status-msg on interaction
osd-margin-y="23"
osd-scale-by-window="yes"
osd-shadow-color="#33000000"
osd-shadow-offset="1"
osd-spacing="0.5"
osd-status-msg='${time-pos} / ${duration}${?percent-pos:　(${percent-pos}%)}${?frame-drop-count:${!frame-drop-count==0:　Dropped: ${frame-drop-count}}}\n${?chapter:Chapter: ${chapter}}'
sub-color="#FFFFFF00"

#############
# Subtitles #
#############

sub-codepage=latin2
sub-auto=all                          # external subs don't have to match the file name exactly to autoload
sub-file-paths-append=ass               # search for external subs in these relative subdirectories
sub-file-paths-append=srt
sub-file-paths-append=sub
sub-file-paths-append=subs
sub-file-paths-append=subtitles

demuxer-mkv-subtitle-preroll            # try to correctly show embedded subs when seeking
embeddedfonts=yes                       # use embedded fonts for SSA/ASS subs
sub-fix-timing=no                       # do not try to fix gaps (which might make it worse in some cases)
sub-ass-force-style=Kerning=yes         # allows you to override style parameters of ASS scripts
sub-use-margins
sub-ass-force-margins

#############
# Languages #
#############

slang=en,eng,pt-br
alang=en,eng,pt-br


#########
# Audio #
#########

ao=pipewire
audio-file-auto=fuzzy                   # external audio doesn't has to match the file name exactly to autoload
audio-pitch-correction=yes              # automatically insert scaletempo when playing with higher speed
volume-max=200                          # maximum volume in %, everything above 100 results in amplification
volume=50                               # default volume, 100 = unchanged

################
# Video Output #
################

vo=gpu # gpu or opengl
hwdec=no
hwdec-codecs=all
gpu-context=auto
tscale=oversample                      # [sharp] oversample <-> linear (triangle) <-> catmull_rom <-> mitchell <-> gaussian <-> bicubic [smooth]
opengl-early-flush=no
opengl-pbo=no                          # "yes" is currently bugged: https://github.com/mpv-player/mpv/issues/4988
icc-profile-auto
###################################
# Protocol Specific Configuration #
###################################

[protocol.https]
cache=yes
#demuxer-max-back-bytes=2147483647
user-agent='Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0'

[protocol.http]
cache=yes
user-agent='Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0'

# Profiles
[best-quality]
profile=gpu-hq
scale=ewa_lanczossharp
cscale=ewa_lanczossharp
interpolation
tscale=oversample
#video-sync=display-resample
hdr-compute-peak=no # enabling bites with video-sync (31.12.2018)
video-sync=display-resample
hr-seek-framedrop=no
tone-mapping=reinhard
