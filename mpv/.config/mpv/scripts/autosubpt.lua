-- requires subliminal, version 1.0 or newer
-- default keybinding: b
-- add the following to your input.conf to change the default keybinding:
-- keyname script_binding auto_load_subs
local utils = require 'mp.utils'
function load_sub_fn()
    subl = "/usr/bin/subliminal" -- use 'which subliminal' to find the path
    mp.msg.info("Procurando legenda...")
    mp.osd_message("Procurando legenda...")
    t = {}
    t.args = {subl, "download", "-s", "-l", "pt-BR", mp.get_property("path")}
    res = utils.subprocess(t)
    if res.status == 0 then
        mp.commandv("rescan_external_files", "reselect")
        mp.msg.info("Sucesso no download da legenda!")
        mp.osd_message("Sucesso no download da legenda!")
    else
        mp.msg.warn("Falha no download da legenda.")
        mp.osd_message("Falha no download da legenda.")
    end
end

mp.add_key_binding("shift+b", "auto_load_subs", load_sub_fn)
