# Iommu on amd processors:
amd_iommu=on iommu=pt 

# Radeonsi+amdgpu post kernel 4.16
radeon.dpm=0 radeon.si_support=0 radeon.cik_support=0 amdgpu.si_support=1 amdgpu.cik_support=1 amdgpu.dc=1 amdgpu.dpm=1

GRUB_DEFAULT=saved
GRUB_SAVEDEFAULT=true
GRUB_TIMEOUT=2
GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
GRUB_CMDLINE_LINUX_DEFAULT="quiet amd_iommu=on iommu=pt radeon.si_support=0 amdgpu.si_support=1 amdgpu.dc=1 radeon.dpm=0 amdgpu.dpm=1"
GRUB_CMDLINE_LINUX="apparmor=1 security=apparmor"
GRUB_THEME=/boot/grub/themes/Elegant-GRUB2/theme.txt
GRUB_PRELOAD_MODULES="lvm"

