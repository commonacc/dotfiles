#!/usr/bin/env bash

cd ~
mv .zhistory .zhistory_bad
strings .zhistory_bad > .zhistory
fc -R .zhistory
