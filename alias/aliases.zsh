# vim set filetype=zsh
# Common
alias ls='ls -lhF --color=auto'
alias sl='ls -lhF --color=auto'
alias ll='ls -l --color=auto'
alias la='ls -lA --color=auto'
alias l='ls -lahF --color=auto'
alias x='startx'

alias dir='dir --color=auto'
alias vdir='vdir --color=auto'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias '..'='cd ../'
alias '...'='cd ../../'
alias '....'='cd ../../../'
alias '.....'='cd ../../../../'

alias ip='ip -color'

# Arch
alias psearch='pacman -Ss'
alias pupdate='sudo pacman -Syu && echo "checkrebuild..." && checkrebuild -v && echo "flatpak update..." && flatpak update -y'
alias pinstall='sudo pacman -S'
alias premove='sudo pacman -Rns'
alias pinfo='pacman -Si'
alias pnotneeded='pacman -Qdtq'

# Debian
alias uu='sudo apt update && sudo apt upgrade'
alias ua='sudo youtube-dl -U && sudo apt update && sudo apt upgrade'
alias aa='sudo apt autoclean && sudo apt autoremove'
alias search='apt search'
alias asearch='apt-cache search'
alias show='apt show'
alias cr='sudo checkrestart'
alias apti='sudo apt install'
alias aptr='sudo apt purge'
alias aptdu='sudo apt dist-upgrade'
alias aptp='apt-cache policy'
alias aptfu='sudo apt full-upgrade'

# handy
alias testdownload='curl -o /dev/null http://cachefly.cachefly.net/100mb.test; true'
alias tunnel='ssh -4 -t -L 5900:localhost:5900'
alias tunnel0='ssh -4 -t -L 5900:localhost:5900'
alias tunnel1='ssh -4 -t -L 5901:localhost:5901'
alias tunnel2='ssh -4 -t -L 5902:localhost:5902'
alias tunnel3='ssh -4 -t -L 5903:localhost:5903'
alias tunnel4='ssh -4 -t -L 5904:localhost:5904'
alias gfx='screenfetch && glxinfo | grep -i "'"vendor\|rendering\|opengl"'" && inxi -F'
alias dfh='df -h --exclude-type=tmpfs --exclude-type=devtmpfs'
alias wined='env WINEARCH=win32 WINEPREFIX=$HOME/.wine32 wine-development'
alias meuip='curl https://icanhazip.com'
alias t=task
alias ret='task due:today +tel add'
alias bc='bc -ql'
alias magnet='aria2c -d ~/Downloads --dht-entry-point=dht.transmissionbt.com:6881 --dht-entry-point=router.bittorrent.com:8991 --dht-entry-point=dht.libtorrent.org:25401 --dht-entry-point=router.bitcomet.com:6881 --enable-dht=true --bt-metadata-only=true --bt-save-metadata=true --listen-port=16886 --dht-listen-port=16885'
alias e='$EDITOR'
alias vimupdate='vim +PlugUpgrade +PlugUpdate +qa'
alias rto='firejail /usr/bin/rtorrent'
alias ytp='yt-dlp -P \~/.local/share/.p/'
alias pacnew="sudo DIFFPROG=meld pacdiff"

# tmux
alias tmu='tmux -2 new-session -A -s main'
alias tma='tmux -2 new-session -A -s second'
alias tmb='tmux -2 new-session -A -s third'
alias ta='tmux -2 attach -t'
alias tad='tmux -2 attach -d -t'
alias ts='tmux -2 new-session -s'
alias tl='tmux list-sessions'
alias tksv='tmux kill-server'
alias tkss='tmux kill-session -t'

# git
alias g='git'
alias ga='git add'
alias gaa='git add --all'
alias gb='git branch'
alias gbl='git blame -b -w'
alias gc='git commit -m'
alias gcl='git clone --recursive'
alias gpristine='git reset --hard && git clean -dfx'
alias gcm='git checkout master'
alias gcd='git checkout develop'
alias gco='git checkout'
alias gd='git diff'
alias gf='git fetch'
alias gfa='git fetch --all --prune'
alias gfo='git fetch origin'
alias glog='git log --graph --pretty=format:"%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset" --abbrev-commit --date=relative --branches'
alias gl='git pull'
alias gm='git merge'
alias gp='git push'
alias gr='git remote'
alias gsi='git submodule init'
alias gss='git status -s'
alias gst='git status'
alias gsu='git submodule update'
alias gpsu='git pull --recurse-submodules && git submodule update --remote --recursive'
# git pull for each directory under $PWD, max depth 1
alias gpr='find . -type d -mindepth 1 -maxdepth 1 -exec git --git-dir={}/.git --work-tree=$PWD/{} pull origin master \;'

# Functions
function cpugov() { 
#	for ((i=0;i<$(nproc);i++)); do 
#		sudo cpufreq-set -c "$i" -r -g "$1"; 
#	done;
	sudo cpupower frequency-set -g "$1";
	cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor; 
}
function vs() { 
	tmux split-window -p 33 "$EDITOR" "$@" || exit; 
}
function mkcd() {
        mkdir -p -- "$1" && cd -- "$1"
}

function cpr() {
        rsync --archive -hh --partial --info=stats1,progress2 --modify-window=1 "$@"
} 
function mvr() {
        rsync --archive -hh --partial --info=stats1,progress2 --modify-window=1 --remove-source-files "$@"
}
