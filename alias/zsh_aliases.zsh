# vim set filetype=zsh
#global zsh alias
alias -g vnclocal='x11vnc -xkb -display :0 -localhost -noxdamage -noxrecord -noxfixes'
alias -g vncsudo='sudo x11vnc -xkb -display :0 -auth /var/run/lightdm/root/:0 -localhost -noxdamage -noxrecord -noxfixes'
alias -g vncsddm='x11vnc -xkb -display :0 -auth ${SDDMXAUTH} -localhost -noxdamage -noxrecord -noxfixes'
