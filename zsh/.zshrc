# vim: set filetype=sh:
# remove all alias
unalias -m '*'

# export useful dirs
if [ -d $HOME/.grepo ]; then
        export GREPO=$HOME/.grepo
fi

if [ -d $HOME/.dotfiles ]; then
        export DOTDIR=$HOME/.dotfiles
fi

# prompt theme
#if [ -f $GREPO/powerless/powerless.zsh ]; then
#        source $GREPO/powerless/powerless.zsh false
#fi

if [ -f $GREPO/agnoster-zsh-theme-no-powerline/agnoster.zsh-theme ]; then
        source $GREPO/agnoster-zsh-theme-no-powerline/agnoster.zsh-theme
fi

# solarized man pages
if [ -f $GREPO/solarized-man/solarized-man.plugin.zsh ]; then
        source $GREPO/solarized-man/solarized-man.plugin.zsh
fi

# fzf search
if [ -f /usr/share/doc/fzf/examples/key-bindings.zsh ]; then
        source /usr/share/doc/fzf/examples/key-bindings.zsh
fi

if [ -f /usr/share/doc/fzf/examples/completion.zsh ]; then
        source /usr/share/doc/fzf/examples/completion.zsh
fi

if [ -f /usr/share/fzf/key-bindings.zsh ]; then
        source /usr/share/fzf/key-bindings.zsh
fi

if [ -f /usr/share/fzf/completion.zsh ]; then
        source /usr/share/fzf/completion.zsh
fi

# delete word stop at delimiter
autoload -U select-word-style
select-word-style bash

# do not interprete comments
setopt interactivecomments
setopt PROMPT_SUBST
setopt auto_cd
setopt longlistjobs
setopt notify
setopt hash_list_all
setopt completeinword
setopt unset
setopt nobeep

# url magick
autoload -Uz bracketed-paste-magic
zle -N bracketed-paste bracketed-paste-magic
autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic

# history
HISTSIZE=10000000
SAVEHIST=10000000
HISTFILE=$HOME/.history
setopt bang_hist
setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt hist_reduce_blanks
setopt hist_save_no_dups
setopt append_history
setopt share_history
unsetopt extended_history
unsetopt hist_beep

# set editor
if [ -n $SSH_CONNECTION ]; then
        export EDITOR='nvim'
else
        export EDITOR='nvim'
fi

## setxkbmap
#if [ `whoami` = "felipe" ]; then
#	setxkbmap -model pc105 -layout us -variant intl -option caps:super,compose:menu
##	#setxkbmap -option caps:super,compose:menu
#fi

# source aliases
#if [ -f ~/.zlocal ]; then
#        source ~/.zlocal
#fi

if [ -f $DOTDIR/alias/aliases.zsh ]; then
        source $DOTDIR/alias/aliases.zsh
fi

if [ -f $DOTDIR/alias/zsh_aliases.zsh ] ; then
        source $DOTDIR/alias/zsh_aliases.zsh
fi

# syntax
if [ -f /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]; then
        source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
        ZSH_HIGHLIGHT_STYLES[comment]='none'
        ZSH_HIGHLIGHT_STYLES[default]='none'
        ZSH_HIGHLIGHT_STYLES[unknown-token]='fg=red'
        ZSH_HIGHLIGHT_STYLES[precommand]='none'
        ZSH_HIGHLIGHT_STYLES[commandseparator]='none'
        ZSH_HIGHLIGHT_STYLES[path]='none'
        ZSH_HIGHLIGHT_STYLES[path_prefix]='none'
        ZSH_HIGHLIGHT_STYLES[path_approx]='fg=yellow'
        ZSH_HIGHLIGHT_STYLES[back-quoted-argument]='none'
        ZSH_HIGHLIGHT_STYLES[assign]='none'
fi

# keybindings
bindkey -e
bindkey '^T'            fzf-history-widget
#bindkey '^R'            history-incremental-pattern-search-backward 
bindkey '^R'            history-incremental-search-backward
bindkey '^S'            history-incremental-search-forward
bindkey '^r'            history-incremental-search-backward
bindkey '^s'            history-incremental-search-forward
bindkey '\e[1;5C'       forward-word                            # C-Right
bindkey '\e[1;5D'       backward-word                           # C-Left
bindkey '\e[2~'         overwrite-mode                          # Insert
bindkey '\e[3~'         delete-char                             # Del
bindkey '\e[5~'         history-search-backward                 # PgUp
bindkey '\e[6~'         history-search-forward                  # PgDn
bindkey '^A'            beginning-of-line                       # Home
bindkey '^D'            delete-char                             # Del
bindkey '^E'            end-of-line                             # End
bindkey '\E[1~' 	beginning-of-line
bindkey '\E[4~' 	end-of-line
bindkey '\E[H' 		beginning-of-line
bindkey '\E[F' 		end-of-line
bindkey '\E[3~'		delete-char

# completions
autoload -U compinit
compinit
zmodload -i zsh/complist
setopt hash_list_all            # hash everything before completion
#setopt completealiases          # complete alisases
setopt always_to_end            # when completing from the middle of a word, move the cursor to the end of the word
setopt complete_in_word         # allow completion from within a word/phrase
setopt correct                  # spelling correction for commands
setopt list_ambiguous           # complete as much of a completion until it gets ambiguous.

unsetopt CASE_GLOB
#setopt GLOB_COMPLETE
setopt AUTO_LIST
setopt extended_glob
#bindkey -M menuselect '^[Z' reverse-menu-complete
bindkey '\e[Z' reverse-menu-complete

zstyle ':completion:*' list-colors "${(@s.:.)LS_COLORS}"
zstyle ':completion:*' use-compctl false

# Enable approximate completions
zstyle ':completion:*:expand:*' tag-order all-expansions

# Automatically update PATH entries
zstyle ':completion:*' rehash true

# Don't insert a literal tab when trying to complete in an empty buffer
zstyle ':completion:*' insert-tab false
zstyle ':completion:*' list-dirs-first true

# Don't try parent path completion if the directories exist
zstyle ':completion:*' accept-exact-dirs true
#zstyle ':completion:*:hosts' hosts off
zstyle -s ':completion:*:hosts' hosts _ssh_config
[ -r ~/.ssh/config ] && _ssh_config+=($(cat ~/.ssh/config | sed -ne 's/Host[=\t ]//p'))
zstyle ':completion:*:hosts' hosts $_ssh_config

# Prettier completion for processes
zstyle ':completion:*:*:*:*:processes' force-list always
zstyle ':completion:*:*:*:*:processes' menu yes select
zstyle ':completion:*:*:*:*:processes' list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01'
zstyle ':completion:*:*:*:*:processes' command "ps -u $USER -o pid,user,args -w -w"

# Use caching to make completion for commands such as dpkg and apt usable.
zstyle ':completion::complete:*' use-cache on
zstyle ':completion::complete:*' cache-path "${XDG_CACHE_HOME}/zsh/compcache"
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

# Group matches and describe.
zstyle ':completion:*:*:*:*:*' menu select
zstyle ':completion:*:matches' group 'yes'
zstyle ':completion:*:options' description 'yes'
zstyle ':completion:*:options' auto-description '%d'
zstyle ':completion:*:corrections' format ' %F{green}-- %d (errors: %e) --%f'
zstyle ':completion:*:descriptions' format ' %F{yellow}-- %d --%f'
zstyle ':completion:*:messages' format ' %F{purple} -- %d --%f'
zstyle ':completion:*:warnings' format ' %F{red}-- no matches found --%f'
zstyle ':completion:*:default' list-prompt '%S%M matches%s'
zstyle ':completion:*' format ' %F{yellow}-- %d --%f'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' verbose yes

# Fuzzy match mistyped completions.
zstyle ':completion:*::::' completer _complete _ignored _approximate
zstyle ':completion:*:match:*' original only
zstyle ':completion:*:approximate:*' max-errors 1 numeric

# Increase the number of errors based on the length of the typed word.
zstyle -e ':completion:*:approximate:*' max-errors 'reply=($((($#PREFIX+$#SUFFIX)/3))numeric)'

# Don't complete unavailable commands.
zstyle ':completion:*:functions' ignored-patterns '(_*|pre(cmd|exec))'

# Array completion element sorting.
zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters

# Directories
#zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*:*:cd:*' tag-order local-directories directory-stack path-directories
zstyle ':completion:*:*:cd:*:directory-stack' menu yes select
zstyle ':completion:*:-tilde-:*' group-order 'named-directories' 'path-directories' 'users' 'expand'
zstyle ':completion:*' squeeze-slashes true

# History
zstyle ':completion:*:history-words' stop yes
zstyle ':completion:*:history-words' remove-all-dups yes
zstyle ':completion:*:history-words' list false
zstyle ':completion:*:history-words' menu yes

# Environmental Variables
zstyle ':completion::*:(-command-|export):*' fake-parameters ${${${_comps[(I)-value-*]#*,}%%,*}:#-*-}

# Don't complete uninteresting users...
zstyle ':completion:*:*:*:users' ignored-patterns \
  adm amanda apache avahi beaglidx bin cacti canna clamav daemon \
  dbus distcache dovecot fax ftp games gdm gkrellmd gopher \
  hacluster haldaemon halt hsqldb ident junkbust ldap lp mail \
  mailman mailnull mldonkey mysql nagios \
  named netdump news nfsnobody nobody nscd ntp nut nx openvpn \
  operator pcap postfix postgres privoxy pulse pvm quagga radvd \
  rpc rpcuser rpm shutdown squid sshd sync uucp vcsa xfs '_*'

# ... unless we really want to.
zstyle '*' single-ignored show

# Ignore multiple entries.
zstyle ':completion:*:(rm|kill|diff):*' ignore-line other
zstyle ':completion:*:rm:*' file-patterns '*:all-files'

# Kill
zstyle ':completion:*:*:*:*:processes' command 'ps -u $LOGNAME -o pid,user,command -w'
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;36=0=01'
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:*:kill:*' force-list always
zstyle ':completion:*:*:kill:*' insert-ids single

# Man
zstyle ':completion:*:manuals' separate-sections true
zstyle ':completion:*:manuals.(^1*)' insert-sections true

if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
        function zle-line-init() {
                echoti smkx
        }
        function zle-line-finish() {
                echoti rmkx
        }
        zle -N zle-line-init
        zle -N zle-line-finish
fi

# Use emacs key bindings
bindkey -e

# [Home] - Go to beginning of line
if [[ -n "${terminfo[khome]}" ]]; then
        bindkey -M emacs "${terminfo[khome]}" beginning-of-line
        bindkey -M viins "${terminfo[khome]}" beginning-of-line
        bindkey -M vicmd "${terminfo[khome]}" beginning-of-line
fi
# [End] - Go to end of line
if [[ -n "${terminfo[kend]}" ]]; then
        bindkey -M emacs "${terminfo[kend]}"  end-of-line
        bindkey -M viins "${terminfo[kend]}"  end-of-line
        bindkey -M vicmd "${terminfo[kend]}"  end-of-line
fi

## fix for st del key
#if [ -n ${terminfo[smkx]} ] && [ -n ${terminfo[rmkx]} ]; then
#        function zle-line-init () { echoti smkx }
#        function zle-line-finish () { echoti rmkx }
#        zle -N zle-line-init
#        zle -N zle-line-finish
#fi

# Local files
if [ -f $HOME/.alias_local ] ; then
        source $HOME/.alias_local
fi

if [ -f $HOME/.alias ]; then
   source $HOME/.alias
fi

if [ -f $HOME/.alias_local ]; then
   source $HOME/.alias_local
fi

if [ -f $HOME/.profile ]; then
        source $HOME/.profile
fi

if [ -x /usr/bin/dircolors ]; then
        test -r $GREPO/dircolors-solarized/dircolors.ansi-universal \
            && eval "$(dircolors -b $GREPO/dircolors-solarized/dircolors.ansi-universal)" \
            || eval "$(dircolors -b)"
fi

function chpwd() {
        emulate -L zsh
        ls -l
}

rga-fzf() {
	RG_PREFIX="rga --files-with-matches"
	local file
	file="$(
		FZF_DEFAULT_COMMAND="$RG_PREFIX '$1'" \
			fzf --sort --preview="[[ ! -z {} ]] && rga --pretty --context 5 {q} {}" \
				--phony -q "$1" \
				--bind "change:reload:$RG_PREFIX {q}" \
				--preview-window="70%:wrap"
	)" &&
	echo "opening $file" &&
	xdg-open "$file"
}

## Function to compress PDF
compress-high() {
    # compress pdf $2 into $1 using minimizing size
    gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$1 $2
}

yt() {
   yt-dlp --get-title --get-url $1 | IFS=$'\n' read -d '' title video audio
mpv --force-media-title=$title --audio-file=<(yt-dlp --http-chunk-size 10M -o - $audio) <(yt-dlp --http-chunk-size 10M -o - $video)
}

# set terminal title
case $TERM in
        xterm*|rxvt|st-256color)
                precmd () {print -Pn "\e]0;%n@%m: %~\a"}
                ;;
esac

stty -ixon      #do not let terminal stop input with ctrl+s

## ssh-agent
#SERVICE="ssh-agent"
#if pgrep -x "$SERVICE" >/dev/null; then
##        echo "$SERVICE is running"
#elif [ ! -S ~/.ssh/ssh_auth_sock ]; then
#	eval $(ssh-agent)
#	ln -sf "$SSH_AUTH_SOCK" ~/.ssh/ssh_auth_sock
#	export SSH_AUTH_SOCK=~/.ssh/ssh_auth_sock
#	ssh-add -l > /dev/null || ssh-add
#fi

SERVICE="autocutsel"
if pgrep -x "$SERVICE" >/dev/null
then
	#echo "$SERVICE is running"
elif [ -x /usr/bin/autocutsel ]; then
	autocutsel -fork
fi
