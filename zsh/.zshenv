# export useful dirs
if [ -d $HOME/.grepo ]; then
        export GREPO=$HOME/.grepo
fi

if [ -d $HOME/.dotfiles ]; then
        export DOTDIR=$HOME/.dotfiles
fi

# env
export EDITOR=nvim
export VISUAL=nvim
export TERMINAL=alacritty
export MAINTERMINAL=alacritty
export ALTTERMINAL=st
export FILEMANAGER=dolphin
export ALTFILEMANAGER=pcmanfm-qt
export MUSICPLAYER=strawberry
export WINEDLLOVERRIDES=winemenubuilder.exe=d
export GTK_OVERLAY_SCROLLING=0
export LIBOVERLAY_SCROLLBAR=0
export SDL_VIDEO_X11_DGAMOUSE=0
export SDL_VIDEO_X11_MOUSEACCEL=1/1/0
export VIRSH_DEFAULT_CONNECT_URI=qemu:///system
export PYGMENTIZE_STYLE=solarized-dark
#export DISMAN_IN_PROCESS=1
export BAT_THEME="Solarized (dark)"
if [[ "$XDG_SESSION_TYPE" == "x11" ]]; then
#    export DISMAN_BACKEND=randr
    export GTK_USE_PORTAL=1
    export GTK_CSD=0
    export XDG_CURRENT_DESKTOP=KDE
    #kde 
    export KWIN_X11_REFRESH_RATE=144000
    export KWIN_X11_NO_SYNC_TO_VBLANK=1
    export KWIN_X11_FORCE_SOFTWARE_VSYNC=0
    export KWIN_TRIPLE_BUFFER=1
elif [[ "$XDG_SESSION_TYPE" == "wayland" ]]; then
    #kde 
    unset KWIN_X11_REFRESH_RATE
    unset KWIN_X11_NO_SYNC_TO_VBLANK
    unset KWIN_X11_FORCE_SOFTWARE_VSYNC
    unset KWIN_TRIPLE_BUFFER
    export DISMAN_BACKEND=qscreen
    export GTK_USE_PORTAL=1
    export GTK_CSD=0
    export MOZ_ENABLE_WAYLAND=1
fi

# ssh-agent
SERVICE="ssh-agent"
if pgrep -x "$SERVICE" >/dev/null; then
#        echo "$SERVICE is running"
elif [ ! -S ~/.ssh/ssh_auth_sock ]; then
	eval $(ssh-agent)
	ln -sf "$SSH_AUTH_SOCK" ~/.ssh/ssh_auth_sock
	export SSH_AUTH_SOCK=~/.ssh/ssh_auth_sock
	ssh-add -l > /dev/null || ssh-add
fi


#export GDK_CORE_DEVICE_EVENTS=1
#export GTK_IM_MODULE=cedilla
#export GTK_IM_MODULE=xim
#export XMODIFIERS=@im=xim
#export QT_IM_MODULE=xim
#export QT4_IM_MODULE=xim
#export CLUTTER_IM_MODULE=xim
#export QT_QPA_PLATFORMTHEME=qt5ct
#export QT_STYLE_OVERRIDE=gtk3
#export QT_STYLE_OVERRIDE=gtk2
#export XDG_MENU_PREFIX="xfce-"
#export BROWSER="firefox -p webext"

#if [ -x rustc ]; then
#        export RUST_SRC_PATH="$(rustc --print sysroot)/lib/rustlib/src/rust/src"
#fi

#setxkbmap
#setxkbmap -model pc105 -layout us -variant intl -option caps:super,compose:menu

. "$HOME/.cargo/env"
