# dotfiles 
<!-- 
  ![Logo](URL to file)
-->

By [commonacc](https://gitlab.com/commonacc).

## Description

**Personal dotfiles.**

Includes zsh, bash, vim, and some small scripts.

## Installation

Clone to ~/.dotfiles:

```
git clone --recursive https://gitlab.com/commonacc/dotfiles ~/.dotfiles
```

Use stow to symlink desired folders:
```
cd ~/.dotfiles
stow alacritty bash gtkrc inputrc mpv profile streamlink taskwarrior tmux vim youtube-dl zsh
```

Optionally you can install aditional repos (oh-my-zsh, hblock, install-gnome-themes, dircolors-solarized) with:
```
scripts/install-update-grepo.sh
```

Inside ssh folder there's a template with secure settings for sshd and ssh. They work with Openssh 7.1 and above. If you wish to install execute the following:
```
sudo cp ~/.dotfiles/ssh/* /etc/ssh
```
