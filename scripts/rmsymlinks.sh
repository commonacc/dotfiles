#!/usr/bin/env bash

dir=~/.dotfiles                    # dotfiles directory
olddir=~/.dotfiles_old             # old dotfiles backup directory
files="vim \
    vimrc \
    inputrc \
    alias_completion.sh \
    bashrc \
    profile \
    taskrc \
    tmux.conf \
    zshrc \
    config/alacritty/alacritty.yml \
    config/mpv/config \
    config/mpv/input.conf \
    config/mpv/scripts/autosub.lua \
    config/mpv/script-opts/osc.conf \
    config/streamlink/config \
    config/youtube-dl.conf"

#    config/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml"
#    zshenv \
#    zsh_aliases \
#    alias \

# create folders if they do not exist
rm -rv $HOME/.config/mpv/
#rm -rv $HOME/.config/xfce4/xfconf/xfce-perchannel-xml
rm -rv $HOME/.config/streamlink
rm -rv $HOME/.config/alacritty

# create dotfiles_old in homedir
echo -n "Creating $olddir for backup of any existing dotfiles in ~ ..."
mkdir -p $olddir
echo "done"

# change to the dotfiles directory
echo -n "Changing to the $dir directory ..."
cd $dir
echo "done"

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks from the homedir to any files in the ~/.dotfiles directory specified in $files
for file in $files; do
    echo "Moving any existing dotfiles from ~ to $olddir"
    if [ -f ~/.$file ]; then
            mv ~/.$file ~/.dotfiles_old/
    fi
    echo "Removing symlink to $file in home directory."
    #ln -s $dir/$file ~/.$file
done

# find broken symlinks
echo -e "\033[31mBroken symlinks:\e[0m"
echo "find ~ -maxdepth 2 -type l -exec test ! -e {} \; -print"
find ~ -maxdepth 2 -type l -exec test ! -e {} \; -print
