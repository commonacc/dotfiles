#!/bin/bash
#recursive git pull

set -euo pipefail

REPODIR=$HOME/.grepo/    # your repo dir

mkdir -p ${REPODIR}
cd ${REPODIR}
for D in *; do
        if [ -d "${D}" ]; then
                cd "${D}"
                echo -e "\e[32mgit pull ${D}\e[0m"
#                git pull
                git pull --recurse-submodules && git submodule update --remote --recursive
                cd ${REPODIR} 
        fi
done
