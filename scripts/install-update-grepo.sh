#!/usr/bin/env bash
set -e
REPODIR=$HOME/.grepo/
REPOS=( \
    "https://gitlab.com/commonacc/hblock.git" \
    "https://gitlab.com/commonacc/install-gnome-themes.git" \
    "https://gitlab.com/commonacc/user.js.git" \
    "https://github.com/seebi/dircolors-solarized" \
    "https://github.com/zlsun/solarized-man" \
    "https://github.com/martinrotter/powerless" \
    "https://gitlab.com/commonacc/agnoster-zsh-theme-no-powerline.git" \
    "https://gitlab.com/commonacc/st-suckless" \
    "https://github.com/itchyny/lightline.vim"
)

mkdir -p ${REPODIR}
cd ${REPODIR}

for x in "${REPOS[@]}"; do
    REPONAME=$(echo -e ${x} | sed -e 's/.*\///' | sed -e 's/\.git//')
    if [ ! -d "$REPONAME" ]; then
        echo -e "\e[32mInstalling  ${REPONAME}\e[0m"
        git clone --recursive ${x}
    else
        echo -e "\e[32mUpdating ${REPONAME}\e[0m"
        cd "${REPONAME}"
        git pull --recurse-submodules
        git submodule update --remote --recursive
        cd ${REPODIR}
    fi
done

echo -e "\e[32mProcesso concluído!\e[0m"

