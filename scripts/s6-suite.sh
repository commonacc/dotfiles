#!/usr/bin/env bash
set -e
REPODIR=$HOME/.grepo/
REPOS=( \
    "git://git.skarnet.org/skalibs" \
    "git://git.skarnet.org/execline" \
    "git://git.skarnet.org/s6" \
    "git://git.skarnet.org/s6-rc" \
    "git://git.skarnet.org/s6-portable-utils" \
    "git://git.skarnet.org/s6-linux-utils" \
    "git://git.skarnet.org/s6-linux-init" \
    "git://git.skarnet.org/s6-dns" \
    "git://git.skarnet.org/s6-networking"
)

mkdir -p ${REPODIR}
cd ${REPODIR}

for x in "${REPOS[@]}"; do
    REPONAME=$(echo -e ${x} | sed -e 's/.*\///' | sed -e 's/\.git//')
    if [ ! -d "$REPONAME" ]; then
        echo -e "\e[32mInstalling  ${REPONAME}\e[0m"
        git clone --recursive ${x}
    else
        echo -e "\e[32mUpdating ${REPONAME}\e[0m"
        cd "${REPONAME}"
        git pull --recurse-submodules
        git submodule update --remote --recursive
        cd ${REPODIR}
    fi
done

echo -e "\e[32mProcesso concluído!\e[0m"

