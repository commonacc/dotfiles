#! /bin/bash
# dynamic-dns-update.sh
set -euo pipefail

ipv6=$(ip -6 a | egrep "inet6.*scope global" --max-count=1 | awk '{print $2}' | awk -F "/" '{print $1}')
curl "https://freedns.afraid.org/dynamic/update.php?[MY-SECRET-TOKEN6]&address=$ipv6"
curl -4 "https://freedns.afraid.org/dynamic/update.php?[MY-SECRET-TOKEN4]"
