#! /bin/bash
# dns-test.sh
set -uo pipefail

tool="drill";
#tool="${dig}";

for domain in duckduckgo.com searx.me startpage.com reddit.com voat.co wikileaks.org mgmarcas.com.br; do 
        
        echo -e "$domain"
        justweb_dns=$(${tool} @138.0.172.30 ${domain} | awk '/msec/{print $4}');
        echo "   justweb: $justweb_dns"
        google1_dns=$(${tool} @8.8.8.8 ${domain} | awk '/msec/{print $4}');
        echo "    google: $google1_dns"
        google2_dns=$(${tool} @8.8.4.4 ${domain} | awk '/msec/{print $4}');
        echo "   google2: $google2_dns"
        cloudflare=$(${tool} @1.1.1.1 ${domain} | awk '/msec/{print $4}');
        echo "cloudflare: $cloudflare"
        cloudflar2=$(${tool} @1.0.0.1 ${domain} | awk '/msec/{print $4}');
        echo "cloudflare2: $cloudflar2"
        quad9=$(${tool} @9.9.9.9 ${domain} | awk '/msec/{print $4}');
        echo "     quad9: $quad9"
        quad92=$(${tool} @149.112.112.112 ${domain} | awk '/msec/{print $4}');
        echo "     quad92: $quad92"
#        opendns1=$(${tool} @208.67.222.222 ${domain} | awk '/msec/{print $4}');
#        echo "   opendns: $opendns1"
#        opendns2=$(${tool} @208.67.220.220 ${domain} | awk '/msec/{print $4}');
#        echo "  opendns2: $opendns2"
#        level3=$(${tool} @4.2.2.2 ${domain} | awk '/msec/{print $4}');
#        echo "    level3: $level3"
#        level3_1=$(${tool} @209.244.0.3 ${domain} | awk '/msec/{print $4}');
#        echo "  level3 1: $level3_1"
#        level3_2=$(${tool} @209.244.0.4 ${domain} | awk '/msec/{print $4}');
#        echo "  level3 2: $level3_2"
        hurricane=$(${tool} @74.82.42.42 ${domain} | awk '/msec/{print $4}');
        echo " hurricane: $hurricane"
        dnswatch1=$(${tool} @84.200.69.80 ${domain} | awk '/msec/{print $4}');
        echo " dnswatch1: $dnswatch1"
        dnswatch2=$(${tool} @84.200.70.40 ${domain} | awk '/msec/{print $4}');
        echo " dnswatch2: $dnswatch2"
        
        echo -e "\\n"
done
