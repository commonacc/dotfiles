#!/usr/bin/env bash
set -euo pipefail

function usb-audio() {

if [ -f $HOME/.asoundrc ]; then
        mv $HOME/.asoundrc $HOME/.asoundrc-backup
fi

cp $DOTDIR/fixes/asoundrc-set-usb ~/.asoundrc

echo "Usb sound card set."
}


function hdmi-audio() {

if [ -f $HOME/.asoundrc ]; then
        mv $HOME/.asoundrc $HOME/.asoundrc-backup
fi

cp $DOTDIR/fixes/asoundrc-hdmi ~/.asoundrc

echo "HDMI sound card set."
}


function onboard-audio() {

if [ -f $HOME/.asoundrc ]; then
        mv $HOME/.asoundrc $HOME/.asoundrc-backup
fi

cp $DOTDIR/fixes/asoundrc-sb-onboard ~/.asoundrc

echo "Onboard sound card set."
}


function pipewire-audio() {

if [ -f $HOME/.asoundrc ]; then
        mv $HOME/.asoundrc $HOME/.asoundrc-backup
fi

cp $DOTDIR/fixes/asoundrc-pipewire ~/.asoundrc

echo "Pipewire backend set."
}


function asoundrc-default() {

if [ -f $HOME/.asoundrc ]; then
        mv $HOME/.asoundrc $HOME/.asoundrc-backup
fi

cp $DOTDIR/fixes/asoundrc-default ~/.asoundrc

echo "Default empty asoundrc set."
}


function help-menu() {
        
echo "Valid options:"
echo "usb | set | usb-audio -> for usb sound card via alsa"
echo "hdmi | vga -> for hdmi sound card via alsa"
echo "onboard | sb | mobo -> for onboard sound card via alsa"
echo "pipewire | pw | pipe -> for default pipewire sink"
echo "default | none | empty | reset | null -> for empty asoundrc"
echo "-h | --help -> show this help menu."
}

case "$1" in
        "usb"|"set"|"usb-audio")
                usb-audio
                ;;
        "hdmi"|"vga")
                hdmi-audio
                ;;
        "onboard"|"sb"|"mobo")
                onboard-audio
                ;;
        "pipewire"|"pw"|"pipe")
                pipewire-audio
                ;;
        "default"|"none"|"empty"|"reset"|"null")
                asoundrc-default
                ;;
        "-h"|"--help")
                help-menu
                ;;
        *)
                echo "Opcão inválida. Para ajuda tente -h ou --help."
                ;;
esac
