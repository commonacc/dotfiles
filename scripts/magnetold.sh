#! /bin/bash
set -euo pipefail
# Convert a magnet URI into a .torrent file

DOWNLOAD_DIR="$HOME/Downloads"
cd $DOWNLOAD_DIR || exit

if [[ -f $(which aria2c) ]] ; then
        # aria2 download only metadata and convert to torrent file
        set +e
        aria2c --conf-path=$HOME/.config/aria2/aria2.conf "$1"
        set -e
else
        # bash implementation
        [[ "$1" =~ xt=urn:btih:([^&/]+) ]] || exit
        hashh=${BASH_REMATCH[1]}
        if [[ "$1" =~ dn=([^&/]+) ]];then
                filename=${BASH_REMATCH[1]}
        else
                filename=$hashh
        fi
        echo "d10:magnet-uri${#1}:${1}e" > "meta-$filename.torrent"
fi
