#! /usr/bin/env sh
# runsway.sh

#export TERMINAL="alacritty"
#export BROWSER="firefox"
export TERMINAL=alacritty
export MAINTERMINAL=alacritty
export ALTTERMINAL=st
export FILEMANAGER=pcmanfm
export ALTFILEMANAGER=pcmanfm-qt
export MUSICPLAYER=strawberry
export WINEDLLOVERRIDES=winemenubuilder.exe=d
#export _JAVA_AWT_WM_NONREPARENTING=1
#export QT_AUTO_SCREEN_SCALE_FACTOR=1
export QT_QPA_PLATFORM=wayland
export QT_QPA_PLATFORMTHEME=qt5ct
#export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
#export GDK_BACKEND=wayland
export MOZ_DBUS_REMOTE=1
export XDG_CURRENT_DESKTOP=sway
export MOZ_ENABLE_WAYLAND=1
export CLUTTER_BACKEND=wayland
export SDL_VIDEODRIVER=wayland

dbus-run-session sway -d -V 2> ~/.local/share/sway/sway-$(date +%F_%T).log #startsway
