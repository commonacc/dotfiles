#!/usr/bin/env bash
# cpugov.sh
# toggle between ondemand & performance governors

set -euo pipefail

set_ondemand() {
#        for ((i=0; i<$(nproc); i++)) do
#		sudo cpufreq-set -c "$i" -r -g "ondemand"
#        done
        sudo cpupower frequency-set -g ondemand
        cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
}

set_performance() {
#        for ((i=0; i<$(nproc); i++)) do
#                sudo cpufreq-set -c "$i" -r -g "performance"
#        done
        sudo cpupower frequency-set -g performance
        cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
}

cpupower frequency-info | grep "governor \"ondemand\""      \
  && set_performance                                        \
  || set_ondemand

