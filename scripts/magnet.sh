#! /bin/bash
set -euo pipefail
# Convert a magnet URI into a .torrent file

DOWNLOAD_DIR="$HOME/Downloads"
cd $DOWNLOAD_DIR || exit

if [[ -f $(which aria2c) ]] ; then
        # aria2 download only metadata and convert to torrent file
        DHT_PORT="16884"
        TORRENT_PORT="16885"
        set +e
        aria2c -d "$DOWNLOAD_DIR" \
                --enable-dht=true \
                --bt-metadata-only=true \
                --bt-save-metadata=true \
                --listen-port="$TORRENT_PORT" \
                --dht-listen-port="$DHT_PORT" \
                --dht-entry-point=dht.transmissionbt.com:6881 \
                --dht-entry-point=router.bittorrent.com:8991 \
                --dht-entry-point=dht.libtorrent.org:25401 \
                --dht-entry-point=router.bitcomet.com:6881 \
                "$1"
        set -e
else
        # bash implementation
        [[ "$1" =~ xt=urn:btih:([^&/]+) ]] || exit
        hashh=${BASH_REMATCH[1]}
        if [[ "$1" =~ dn=([^&/]+) ]];then
                filename=${BASH_REMATCH[1]}
        else
                filename=$hashh
        fi
        echo "d10:magnet-uri${#1}:${1}e" > "meta-$filename.torrent"
fi
