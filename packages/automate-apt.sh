#!/bin/bash
set -euo pipefail

if command -v apt > /dev/null; then
    echo "apt detected."
    echo "Installing packages from $1"
    xargs -a <(awk '! /^ *(#|$)/' "$1") -r -- sudo apt install
    echo "Done!"
elif command -v pacman > /dev/null; then
    echo "pacman detected."
    echo "Installing packages from $1"
    xargs -a <(awk '! /^ *(#|$)/' "$1") -r -- sudo pacman -S
    echo "Done!"
fi
