#! /bin/bash
# [:VIM_EVAL:]expand('%:t')[:END_EVAL:]
set -euo pipefail

readonly PROGNAME=$(basename "$0")
readonly PROGDIR=$(readlink -m "$(dirname "$0")")
readonly ARGS=("$@")

#readonly DIR=$(dirname "$(readlink -f "$0")")
#if [ -e "${DIR}" ]; then
#    readonly WORK_DIR=$(mktemp -d -p "${DIR}")
#fi

