#! /usr/bin/env sh
# [:VIM_EVAL:]expand('%:t')[:END_EVAL:]
set -euo pipefail

readonly PROGNAME=$(basename "$0")
readonly PROGDIR=$(readlink -m "$(dirname "$0")")
readonly ARGS=("$@")

#readonly DIR=$(dirname "$(readlink -f "$0")")
#if [ -e "${DIR}" ]; then
#    readonly WORK_DIR=$(mktemp -d -p "${DIR}")
#fi

#function interrupt {
#   echo "Removing temporary directory ${WORK_DIR}"
#   rm -rf "${WORK_DIR}"
#}
#trap interrupt INT SIGINT TERM SIGTERM EXIT

