if has("autocmd")
    augroup templates
        au!
        " read in template files
        autocmd BufNewFile *.* silent! execute '0r $HOME/.vim/templates/skel.'.expand("<afile>:e")
        " parse special text in the templates after the read
        autocmd BufNewFile * %substitute#\[:VIM_EVAL:\]\(.\{-\}\)\[:END_EVAL:\]#\=eval(submatch(1))#ge
        " Disables automatic commenting on newline
        autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
    augroup END
    augroup src_config_files
        au!
        " Update binds when sxhkdrc is updated.
        autocmd BufWritePost *sxhkdrc !pkill -USR1 -x sxhkd
        " Source zshrc when it is updated.
        autocmd BufWritePost *vimrc source ${HOME}/.vimrc | setlocal filetype=vim
        autocmd BufWritePost *zshrc,*aliases.zsh source ${HOME}/.zshrc |
        setlocal filetype=zsh
    augroup END
endif

"filetype indent on
"filetype on
"filetype plugin on

let mapleader = " " " map space as leader

inoremap jj <Esc>
map ; :
map <CR><Leader> ^i<CR><Esc>
map <F2> :NERDTreeToggle<CR>
map <Leader><CR> A<CR><Esc>
noremap ;; ;
:command -nargs=0 -range Sortwordsinline <line1>,<line2>call setline('.',join(sort(split(getline('.'),' ')),' '))

"tab completion
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Replace all is aliased to S
nnoremap S :%s//g<Left><Left>
set pastetoggle=<F3>

set autoindent " match indent
set autoread " read the file on changes
set background=dark
set backspace=indent,eol,start " set backspace rules
set clipboard=unnamedplus " use system clipboard
set copyindent " copy previous indent
set cursorline " highlight current line
set expandtab " converts tab to spaces
set guifont=Monospace\ 14 " font for gvim
set history=10000
set hlsearch " highlight search results
set ignorecase " search is case insensitive
set incsearch " search as we type
set laststatus=2 " always show status line
set matchtime=200 " for 200ms
set mouse=a " enable mouse support
set nobackup " disable backup files
set nocompatible " go to 21st century features
set noerrorbells " disable error bells
set nohlsearch " disable highlighting of search results
set noswapfile " disable swap files
set number " show the actual current line number
set path+=** " Search sub folders and tab completion for related
set norelativenumber " show relative line numbers
set ruler " cursor position in status bar
set scrolloff=7 " keep cursor X lines from screen border
set shiftwidth=4 " indent width
set shortmess+=I " don't nag about children in uganda
set showcmd " show commands as we type
set showmatch " show matching bracket
set showmode " show current mode
set smartcase " Ai anyone?
set splitbelow splitright " Splits open at the bottom and right
set t_Co=256
set tabstop=4 " tab columns
set visualbell " enable visual bell
set wildmenu " Display all matching when tab complete
set wildmode=longest,list,full " Enable autocompletion
set nomodeline " vulnerable to execute arbitrary commands
" lazy drawing
set lazyredraw
set ttyfast
filetype plugin indent on
syntax enable
syntax on

" tmux and alacritty true colors fix
if exists('+termguicolors')
  let &t_8f="\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b="\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif

hi CursorLine cterm=NONE ctermbg=black ctermfg=darkgreen guibg=black ctermfg=darkgreen

" plugin dir vim/plugged
call plug#begin('~/.vim/plugged')
":Helptags
"Plug 'https://github.com/scrooloose/nerdtree'
Plug 'https://github.com/vim-syntastic/syntastic'
Plug 'https://github.com/itchyny/lightline.vim'
"Plug 'https://github.com/artur-shaik/vim-javacomplete2'
Plug 'https://github.com/rust-lang/rust.vim'
"Plug 'https://github.com/altercation/vim-colors-solarized'
"Plug 'https://github.com/arcticicestudio/nord-vim'
Plug 'https://github.com/machakann/vim-highlightedyank'
Plug 'https://github.com/neoclide/coc.nvim', {'branch': 'release'}
Plug 'https://github.com/frankier/neovim-colors-solarized-truecolor-only'
":PlugInstall
call plug#end()

"rustfmt on save with rust.vim plugin
let g:autofmt_autosave = 1

"rust completion
let g:ycm_rust_src_path = $RUST_SRC_PATH

" vim plug download if not installed
if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source ~/.vimrc
endif

" syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

set termguicolors
"let g:solarized_termcolors=256
"let g:solarized_termtrans = 1 " This gets rid of the grey background
set background=dark
colorscheme solarized

""java completion
"autocmd FileType java setlocal omnifunc=javacomplete#Complete
""To enable smart (trying to guess import option) inserting class imports with F4, add:
"nmap <F4> <Plug>(JavaComplete-Imports-AddSmart)
"imap <F4> <Plug>(JavaComplete-Imports-AddSmart)
""To enable usual (will ask for import option) inserting class imports with F5, add:
"nmap <F5> <Plug>(JavaComplete-Imports-Add)
"imap <F5> <Plug>(JavaComplete-Imports-Add)
""To add all missing imports with F6:
"nmap <F6> <Plug>(JavaComplete-Imports-AddMissing)
"imap <F6> <Plug>(JavaComplete-Imports-AddMissing)
""To remove all unused imports with F7:
"nmap <F7> <Plug>(JavaComplete-Imports-RemoveUnused)
"imap <F7> <Plug>(JavaComplete-Imports-RemoveUnused)
"nmap <leader>jI <Plug>(JavaComplete-Imports-AddMissing)
"nmap <leader>jR <Plug>(JavaComplete-Imports-RemoveUnused)
"nmap <leader>ji <Plug>(JavaComplete-Imports-AddSmart)
"nmap <leader>jii <Plug>(JavaComplete-Imports-Add)
"imap <C-j>I <Plug>(JavaComplete-Imports-AddMissing)
"imap <C-j>R <Plug>(JavaComplete-Imports-RemoveUnused)
"imap <C-j>i <Plug>(JavaComplete-Imports-AddSmart)
"imap <C-j>ii <Plug>(JavaComplete-Imports-Add)
"nmap <leader>jM <Plug>(JavaComplete-Generate-AbstractMethods)
"imap <C-j>jM <Plug>(JavaComplete-Generate-AbstractMethods)
"nmap <leader>jA <Plug>(JavaComplete-Generate-Accessors)
"nmap <leader>js <Plug>(JavaComplete-Generate-AccessorSetter)
"nmap <leader>jg <Plug>(JavaComplete-Generate-AccessorGetter)
"nmap <leader>ja <Plug>(JavaComplete-Generate-AccessorSetterGetter)
"nmap <leader>jts <Plug>(JavaComplete-Generate-ToString)
"nmap <leader>jeq <Plug>(JavaComplete-Generate-EqualsAndHashCode)
"nmap <leader>jc <Plug>(JavaComplete-Generate-Constructor)
"nmap <leader>jcc <Plug>(JavaComplete-Generate-DefaultConstructor)
"imap <C-j>s <Plug>(JavaComplete-Generate-AccessorSetter)
"imap <C-j>g <Plug>(JavaComplete-Generate-AccessorGetter)
"imap <C-j>a <Plug>(JavaComplete-Generate-AccessorSetterGetter)
"vmap <leader>js <Plug>(JavaComplete-Generate-AccessorSetter)
"vmap <leader>jg <Plug>(JavaComplete-Generate-AccessorGetter)
"vmap <leader>ja <Plug>(JavaComplete-Generate-AccessorSetterGetter)
"nmap <silent> <buffer> <leader>jn <Plug>(JavaComplete-Generate-NewClass)
"nmap <silent> <buffer> <leader>jN <Plug>(JavaComplete-Generate-ClassInFile)

