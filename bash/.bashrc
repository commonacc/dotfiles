# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# export useful dirs
if [ -d $HOME/.grepo ]; then
        export GREPO=$HOME/.grepo
fi

if [ -d $HOME/.dotfiles ]; then
        export DOTDIR=$HOME/.dotfiles
fi

# don't put duplicate lines or lines starting with space in the history.
HISTCONTROL=ignoreboth:erasedups
HISTIGNORE="dir:ll:la:ls:x:..*"

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000000
HISTFILESIZE=1000000

# Enable the useful Bash 4 features:
#  - autocd, no need to type 'cd' when changing directory
#  - cdspell, automatically fix small directory typos when changing directory
#  - globstar, ** recursive glob
#  - histappend, append to history, don't overwrite
#  - nocaseglob, case-insensitive globbing
shopt -s autocd cdspell globstar histappend nocaseglob

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# Enable history expansion with space.
bind Space:magic-space

# Set the appropriate umask.
umask 002

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
#less secure
unset LESSOPEN
unset LESSPIPE
export LESSSECURE=1

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# color prompt if supported
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'
fi

if [ -f $HOME/.dotfiles/zsh/aliases.zsh ]; then
    . $HOME/.dotfiles/zsh/aliases.zsh
fi

if [ -f ~/.alias_local ]; then
    . ~/.alias_local
fi

# enable programmable completion features (you don't need to enable
#bind 'set completion-ignore-case on'
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# cd with ls -l
function cd {
    dir="${@:-$HOME}"  # ~ isn't expanded when in quotes
    [ -z "${dir}" ] && dir=~
    if ! builtin cd "$dir"
    then
        dir=`compgen -d "${dir}" | head -1`
        if builtin cd "$dir"
        then
            clear
            pwd
            ls -l
        fi
    else
        clear
        pwd
        ls -l
    fi
}

function go(){
	if (("$#" > 0)); then
		pushd "$@" > /dev/null
		ls -l
	else
		cd $HOME
		ls -l
	fi
}

if [ -f $HOME/.dotfiles/alias_completion.sh ]; then
    source $HOME/.dotfiles/alias_completion.sh
fi

PROMPT_COMMAND='history -a'
export TERM=xterm-256color
export EDITOR=nvim
export WINEDLLOVERRIDES=winemenubuilder.exe=d
export GTK_IM_MODULE=cedilla
export QT_IM_MODULE=xim
export QT_QPA_PLATFORMTHEME=qt5ct
#export QT_STYLE_OVERRIDE=gtk2
export SDL_VIDEO_X11_DGAMOUSE=0
export SDL_VIDEO_X11_MOUSEACCEL=1/1/0
export XDG_MENU_PREFIX="xfce-"
export VIRSH_DEFAULT_CONNECT_URI=qemu:///system

stty -ixon
. "$HOME/.cargo/env"
